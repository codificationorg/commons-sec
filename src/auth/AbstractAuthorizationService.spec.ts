import * as test from 'tape';

import { LoggerFactory, Level } from '@codificationorg/commons-logging/lib';
import { HttpRequest } from '@codificationorg/commons-http';

import { AbstractAuthorizationService } from './AbstractAuthorizationService';
import { AuthorizationService } from './AuthorizationService';
import { User } from './User';

test('AbstractAuthorizationService Unit Tests', t => {
  LoggerFactory.GLOBAL_LEVEL = Level.ALL;

  const result: AuthorizationService = new TestableAbstractAuthorizationService(
    'admins',
    'users',
    'editors'
  );

  t.is(
    result.isMemberOfAny(null, 'users', 'foos'),
    true,
    'returns true for membership in any supplied group'
  );
  t.is(
    result.isMemberOfAny(null, 'managers', 'foos'),
    false,
    'returns false for membership in any supplied group'
  );
  t.is(
    result.isMemberOfAll(null, 'users', 'foos'),
    false,
    'returns false for membership in all supplied grousp'
  );
  t.is(
    result.isMemberOfAll(null, 'users', 'editors'),
    true,
    'returns true for membership in all supplied groups'
  );

  t.end();
});

class TestableAbstractAuthorizationService extends AbstractAuthorizationService {
  private groups: string[];

  constructor(...groups: string[]) {
    super();
    this.groups = groups;
  }

  findUser(request: HttpRequest): User {
    return {
      name: 'Joe Smith',
      username: 'foo',
      email: 'foo@smith.net',
      groups: this.groups
    };
  }
}
