import { HttpRequest } from '@codificationorg/commons-http';

import { AuthorizationService } from './AuthorizationService';
import { User } from './User';

export abstract class AbstractAuthorizationService
  implements AuthorizationService {
  public isMemberOfAny(request: HttpRequest, ...groups: string[]): boolean {
    return this.membershipCount(request, ...groups) > 0;
  }

  public isMemberOfAll(request: HttpRequest, ...groups: string[]): boolean {
    return this.membershipCount(request, ...groups) == groups.length;
  }

  private membershipCount(request: HttpRequest, ...groups: string[]): number {
    return this.findGroups(request).filter(group => groups.indexOf(group) > -1)
      .length;
  }

  private findGroups(request: HttpRequest): string[] {
    const user = this.findUser(request);
    return user ? user.groups : [];
  }

  abstract findUser(request: HttpRequest): User;
}
