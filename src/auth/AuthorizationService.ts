import { HttpRequest } from '@codificationorg/commons-http';

import { User } from './User';

export interface AuthorizationService {
  isMemberOfAny(request: HttpRequest, ...groups: string[]): boolean;

  isMemberOfAll(request: HttpRequest, ...groups: string[]): boolean;

  findUser(request: HttpRequest): User;
}
