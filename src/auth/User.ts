export interface User {
  name: string;
  username: string;
  email: string;
  groups: string[];
}
